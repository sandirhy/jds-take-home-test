from flask import Blueprint, jsonify
from db import db
from flask_sqlalchemy import SQLAlchemy
from handlers.chart_handler import ChartHandler
from middlewares.auth import check_token

api_bp = Blueprint('api', __name__,url_prefix='/api')

@api_bp.app_errorhandler(404)
def handle_404(err):
    return jsonify({'error': 'Not found'}), 404

@api_bp.app_errorhandler(500)
def handle_500(err):
    return jsonify({'error': 'Internal Server Error'}), 500

@api_bp.route('/angka-harapan-hidup/tahunan')
def get_angka_harapan_hidup():
    return ChartHandler.get_anual_ahh()

@api_bp.route('/indeks-kesehatan/tahunan')
@check_token
def get_indeks_kesehatan():
    return ChartHandler.get_anual_ik()

@api_bp.route('/perbandingan-ahh-dan-ik')
@check_token
def get_ahh_ik_comparison():
    return ChartHandler.get_ahh_ik_comparison()

@api_bp.route('/perbandingan-ahh-dan-ik/<kode_kab_kota>')
@check_token
def get_ahh_ik_kota_comparison(kode_kab_kota):
    return ChartHandler.get_ahh_ik_kota_comparison(kode_kab_kota)
