import os
from flask import Flask,jsonify
from flask_migrate import Migrate
from dotenv import load_dotenv
from seeder.seeds import seed_data
from db import db
from resources.charts import api_bp


load_dotenv()

app = Flask(__name__)
app.app_context().push()
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('DATABASE_URL')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)
migrate = Migrate(app, db)

seed_data()

app.register_blueprint(api_bp)

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')