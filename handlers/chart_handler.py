from services.angka_harapan_hidup_service import AngkaHarapanHidupService
from services.indeks_kesehatan_service import IndeksKesehatanService
from flask import jsonify

ahh_service = AngkaHarapanHidupService()
ik_service = IndeksKesehatanService()
class ChartHandler:
    def get_anual_ahh():
        data = ahh_service.get_anual_data()
        return jsonify(
            {
                'data': data
            })
    def get_anual_ik():
        data = ik_service.get_anual_data()
        return jsonify(
            {
                'data': data
            })
    def get_ahh_ik_comparison():
        data = ahh_service.get_ahh_ik_comparison()
        return jsonify(
            {
                'data': data
            })
    def get_ahh_ik_kota_comparison(kode_kab_kota):
        data = ahh_service.get_ahh_ik_kota_comparison(kode_kab_kota)
        return jsonify(
            {
                'data': data
            })