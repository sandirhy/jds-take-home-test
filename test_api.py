import unittest
import requests
import os
from dotenv import load_dotenv

load_dotenv()
class TestAPI(unittest.TestCase):
    headers = {
        'Authorization': 'Bearer '+str(os.getenv('AUTH_TOKEN'))
    }
    def test_angka_harapan_hidup_anual(self):
        response = requests.get(str(os.getenv('API_URL'))+'angka-harapan-hidup/tahunan', headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()['data']), 12)
        print("Test api angka harapan hidup tahunan success")

    def test_indeks_kesehatan_anual(self):
        response = requests.get(str(os.getenv('API_URL'))+'indeks-kesehatan/tahunan', headers=self.headers)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()['data']), 12)
        print("Test api indeks kesehatan tahunan success")

    def test_perbandingan_ahh_dan_ikk(self):
        response = requests.get(str(os.getenv('API_URL'))+'perbandingan-ahh-dan-ik', headers=self.headers)
        self.assertEqual(response.status_code, 200)
        print("Test api perbandingan ahh dan ikk success")

    def test_perbandingan_ahh_dan_ikk_kota(self):
        response = requests.get(str(os.getenv('API_URL'))+'perbandingan-ahh-dan-ik/3201', headers=self.headers)
        self.assertEqual(response.status_code, 200)
        print("Test api perbandingan ahh dan ikk per kota success")

if __name__ == '__main__':
    tester = TestAPI()
    tester.test_angka_harapan_hidup_anual()
    tester.test_indeks_kesehatan_anual()
    tester.test_perbandingan_ahh_dan_ikk()
    tester.test_perbandingan_ahh_dan_ikk_kota()