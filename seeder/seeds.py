import json
from models.indeks_kesehatan import IndeksKesehatan
from models.angka_harapan_hidup import AngkaHarapanHidup
from db import db

def seed_data():
    print('Seeding data...')
    seed_indeks_kesehatan()
    seed_angka_harapan_hidup()

def seed_indeks_kesehatan():
    db.reflect()
    exist = db.session.execute("SELECT EXISTS ( SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename  = 'indeks_kesehatan')").fetchone()[0]
    if exist:
        print("Exist")
        if db.session.query(IndeksKesehatan).count() > 0:
            print('Data indeks kesehatan already exists, skipped.')
            return
        with open('./seeder/indeks_kesehatan.json') as f:
            data = json.load(f)
            for indeksKesehatan in data['data']:
                indeksKesehatan = IndeksKesehatan(
                    kode_provinsi=indeksKesehatan['kode_provinsi'],
                    nama_provinsi=indeksKesehatan['nama_provinsi'],
                    kode_kota_kab=indeksKesehatan['kode_kabupaten_kota'],
                    nama_kota_kab=indeksKesehatan['nama_kabupaten_kota'],
                    indeks_kesehatan=indeksKesehatan['indeks_kesehatan'],
                    satuan=indeksKesehatan['satuan'],
                    tahun=indeksKesehatan['tahun']
                )
                db.session.add(indeksKesehatan)
            db.session.commit()
            print('Data indeks kesehatan seeded.')

def seed_angka_harapan_hidup():
    db.reflect()
    exist = db.session.execute("SELECT EXISTS ( SELECT FROM pg_tables WHERE schemaname = 'public' AND tablename  = 'angka_harapan_hidup')").fetchone()[0]
    if exist:
        if db.session.query(AngkaHarapanHidup).count() > 0:
            print('Data angka harapan hidup already exists, skipped.')
            return
        with open('./seeder/angka_harapan_hidup.json') as f:
            data = json.load(f)
            for angkaHarapanHidup in data['data']:
                angkaHarapanHidup = AngkaHarapanHidup(
                    kode_provinsi=angkaHarapanHidup['kode_provinsi'],
                    nama_provinsi=angkaHarapanHidup['nama_provinsi'],
                    kode_kota_kab=angkaHarapanHidup['kode_kabupaten_kota'],
                    nama_kota_kab=angkaHarapanHidup['nama_kabupaten_kota'],
                    angka_harapan_hidup=angkaHarapanHidup['angka_harapan_hidup'],
                    satuan=angkaHarapanHidup['satuan'],
                    tahun=angkaHarapanHidup['tahun']
                )
                db.session.add(angkaHarapanHidup)
            db.session.commit()
            print('Data angka harapan hidup seeded.')