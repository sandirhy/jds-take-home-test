from functools import wraps
from flask import request, jsonify
import os

def check_token(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'Authorization' not in request.headers:
            return jsonify({"message": "Missing Authorization Header"}), 401
        else:
            try:
                token = request.headers['Authorization'].split(" ")[1]
                if token != os.getenv("AUTH_TOKEN"):
                    return jsonify({"message": "Invalid Token"}), 401
            except:
                return jsonify({"message": "Invalid Token"}), 401
            return f(*args, **kwargs)
    return wrap
