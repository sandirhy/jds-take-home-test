"""angka_harapan_hidup table

Revision ID: b81ebad96f25
Revises: ca7df5d89690
Create Date: 2023-01-15 20:59:43.416571

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b81ebad96f25'
down_revision = 'ca7df5d89690'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('angka_harapan_hidup',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('kode_provinsi', sa.String(length=2), nullable=False),
    sa.Column('nama_provinsi', sa.String(length=50), nullable=False),
    sa.Column('kode_kota_kab', sa.String(length=4), nullable=False),
    sa.Column('nama_kota_kab', sa.String(length=50), nullable=False),
    sa.Column('angka_harapan_hidup', sa.Float(), nullable=False),
    sa.Column('satuan', sa.String(length=10), nullable=False),
    sa.Column('tahun', sa.Integer(), nullable=False),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('angka_harapan_hidup')
    # ### end Alembic commands ###
