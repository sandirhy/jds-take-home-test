from db import db

class AngkaHarapanHidupRepo:
    def get_anual_data():
        result = db.session.execute('SELECT kode_provinsi,nama_provinsi,tahun, round(avg(angka_harapan_hidup)::numeric,2) as angka_harapan_hidup FROM angka_harapan_hidup group by kode_provinsi,nama_provinsi,tahun order by tahun asc')
        data = result.fetchall()
        list = []
        for row in data:
            list.append({
                'kode_provinsi': row[0],
                'nama_provinsi': row[1],
                'tahun': row[2],
                'angka_harapan_hidup': row[3],
            })
        return list
    
    def get_perbandingan_ahh_dan_ik():
        result = db.session.execute('SELECT a.kode_provinsi,a.nama_provinsi,a.tahun, round(avg(a.angka_harapan_hidup)::numeric,2) as angka_harapan_hidup, round(avg(b.indeks_kesehatan)::numeric,2) FROM angka_harapan_hidup a join indeks_kesehatan b on a.kode_provinsi = b.kode_provinsi and a.tahun = b.tahun  group by a.kode_provinsi, a.nama_provinsi, a.tahun order by a.tahun asc')
        data = result.fetchall()
        list = []
        for row in data:
            list.append({
                'kode_provinsi': row[0],
                'nama_provinsi': row[1],
                'tahun': row[2],
                'angka_harapan_hidup': row[3],
                'indeks_kesehatan': row[4],
            })
        return list
    def get_perbandingan_ahh_dan_ik_kota(kode_kab_kota):
        result = db.session.execute("SELECT a.kode_provinsi,a.nama_provinsi,a.nama_kota_kab,a.tahun, round(avg(a.angka_harapan_hidup)::numeric,2) as angka_harapan_hidup, round(avg(b.indeks_kesehatan)::numeric,2) FROM angka_harapan_hidup a join indeks_kesehatan b on a.kode_provinsi = b.kode_provinsi and a.tahun = b.tahun where a.kode_kota_kab = '"+kode_kab_kota+"' group by a.kode_provinsi, a.nama_provinsi, a.tahun, a.nama_kota_kab order by a.tahun asc")
        data = result.fetchall()
        list = []
        for row in data:
            list.append({
                'kode_provinsi': row[0],
                'nama_provinsi': row[1],
                'nama_kabupaten_kota': row[2],
                'tahun': row[3],
                'angka_harapan_hidup': row[4],
                'indeks_kesehatan': row[5],
            })
        return list