from db import db

class IndeksKesehatanRepo:
    def get_anual_data():
        result = db.session.execute('SELECT kode_provinsi,nama_provinsi,tahun, round(avg(indeks_kesehatan)::numeric,2) as indeks_kesehatan FROM indeks_kesehatan group by kode_provinsi,nama_provinsi,tahun order by tahun asc')
        data = result.fetchall()
        list = []
        for row in data:
            list.append({
                'kode_provinsi': row[0],
                'nama_provinsi': row[1],
                'tahun': row[2],
                'indeks_kesehatan': row[3],
            })
        return list
    