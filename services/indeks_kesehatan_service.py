from repositories.indeks_kesehatan_repo import IndeksKesehatanRepo

class IndeksKesehatanService:
    def __init__(self) -> None:
        self.angka_harapan_hidup_repository = IndeksKesehatanRepo
    def get_anual_data(self):
        return IndeksKesehatanRepo.get_anual_data()