from repositories.angka_harapan_hidup_repo import AngkaHarapanHidupRepo

class AngkaHarapanHidupService:
    def __init__(self) -> None:
        self.angka_harapan_hidup_repository = AngkaHarapanHidupRepo
    def get_anual_data(self):
        return AngkaHarapanHidupRepo.get_anual_data()
    def get_ahh_ik_comparison(self):
        return AngkaHarapanHidupRepo.get_perbandingan_ahh_dan_ik()
    def get_ahh_ik_kota_comparison(self, kode_kab_kota):
        return AngkaHarapanHidupRepo.get_perbandingan_ahh_dan_ik_kota(kode_kab_kota)