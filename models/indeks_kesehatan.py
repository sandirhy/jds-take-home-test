from db import db

class IndeksKesehatan(db.Model):
    __tablename__ = 'indeks_kesehatan'
    id = db.Column(db.Integer, primary_key=True)
    kode_provinsi = db.Column(db.String(2), nullable=False)
    nama_provinsi = db.Column(db.String(50), nullable=False)
    kode_kota_kab = db.Column(db.String(4), nullable=False)
    nama_kota_kab = db.Column(db.String(50), nullable=False)
    indeks_kesehatan = db.Column(db.Float, nullable=False)
    satuan = db.Column(db.String(10), nullable=False)
    tahun = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return f'IndeksKesehatan({self.kode_provinsi}, {self.nama_provinsi}, {self.kode_kota_kab}, {self.nama_kota_kab}, {self.indeks_kesehatan}, {self.satuan}, {self.tahun})'