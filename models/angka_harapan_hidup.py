from db import db

class AngkaHarapanHidup(db.Model):
    __tablename__ = 'angka_harapan_hidup'
    id = db.Column(db.Integer, primary_key=True)
    kode_provinsi = db.Column(db.String(2), nullable=False)
    nama_provinsi = db.Column(db.String(50), nullable=False)
    kode_kota_kab = db.Column(db.String(4), nullable=False)
    nama_kota_kab = db.Column(db.String(50), nullable=False)
    angka_harapan_hidup = db.Column(db.Float, nullable=False)
    satuan = db.Column(db.String(10), nullable=False)
    tahun = db.Column(db.Integer, nullable=False)

    def __repr__(self):
        return f'AngkaHarapanHidup({self.kode_provinsi}, {self.nama_provinsi}, {self.kode_kota_kab}, {self.nama_kota_kab}, {self.angka_harapan_hidup}, {self.satuan}, {self.tahun})'