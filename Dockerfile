# Use an official Python runtime as the base image
FROM python:3.11-alpine
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
ENV FLASK_RUN_PORT=5000
# Set the working directory in the container
WORKDIR /app

# Copy the requirements file into the container
COPY requirements.txt .
# Install the application dependencies
RUN pip3 install --no-cache-dir -r requirements.txt

# Copy the application code into the container
COPY . .

# Expose the port on which the application will run
EXPOSE 5000

# Start the application
COPY cmds.sh /cmds.sh
RUN ["chmod", "+x", "/cmds.sh"]
ENTRYPOINT ["/cmds.sh"]
# ENTRYPOINT FLASK_APP=/app/app.py flask run --host=0.0.0.0